package com.test.retail.models;

import com.test.retail.utilities.Currency;
import com.test.retail.utilities.ProductType;

/**
 * 
 * @author Hasan
 *
 */
public class Product {

	private String productName;
	private double productPrice;
	private ProductType productType;
	private Currency currency;

	/**
	 * empty constructor
	 */
	public Product() {

	}

	/**
	 * Product class constructor
	 * 
	 * @param productName
	 * @param productPrice
	 * @param productType
	 * @param currency
	 */
	public Product(String productName, double productPrice, ProductType productType, Currency currency) {
		super();
		this.productName = productName;
		this.productType = productType;
		if (currency == Currency.USD) {
			this.productPrice = productPrice;
			this.currency = currency;
		} else {
			convertProductPriceToUSD(productPrice, currency);
		}
	}

	/**
	 * Converts the product price from any supported currency into USD
	 * 
	 * @param productPrice
	 * @param currency
	 */
	private void convertProductPriceToUSD(double productPrice, Currency currency) {
		switch (currency) {
		case LB:
			this.currency = Currency.USD;
			this.productPrice = productPrice / 1500;
			break;
		default:
			break;
		}
	}

	/**
	 * product name getter
	 * 
	 * @return productName
	 */
	public String getProductName() {
		return productName;
	}

	/**
	 * product name setter
	 * 
	 * @param productName
	 */
	public void setProductName(String productName) {
		this.productName = productName;
	}

	/**
	 * product price getter
	 * 
	 * @return productPrice
	 */
	public double getProductPrice() {
		return productPrice;
	}

	/**
	 * product price setter
	 * 
	 * @param productPrice
	 */
	public void setProductPrice(double productPrice) {
		this.productPrice = productPrice;
	}

	/**
	 * product type getter
	 * 
	 * @return productType
	 */
	public ProductType getProductType() {
		return productType;
	}

	/**
	 * product type setter
	 * 
	 * @param productType
	 */
	public void setProductType(ProductType productType) {
		this.productType = productType;
	}

	/**
	 * currency getter
	 * 
	 * @return currency
	 */
	public Currency getCurrency() {
		return currency;
	}

}
