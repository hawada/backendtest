package com.test.retail.models;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Hasan
 * An abstract class that represents a retail application user, it contains common
 * properties and functions that all users should have.
 */
abstract public class User {
	
	private String firstName;
	private String lastName;
	private String email;
	private String userName;
	private String password;
	private List<Bill> bills;

	/**
	 * empty constructor
	 */
	public User() {

	}

	/**
	 * User class constructor
	 * @param firstName
	 * @param lastName
	 * @param email
	 * @param userName
	 * @param password
	 * @param bills
	 */
	public User(String firstName, String lastName, String email, String userName, String password, List<Bill> bills) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.userName = userName;
		this.password = password;
		this.bills = bills != null ? bills : new ArrayList<>();
	}

	/**
	 * This method should be overridden by class direct children to specify 
	 * the discount amount which the discount is going to be based on
	 * @return
	 */
	abstract double getDiscountPercentage();

	/**
	 * firstName getter
	 * @return firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * firstName setter
	 * @param firstName
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * lastName getter
	 * @return lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * lastName setter
	 * @param lastName
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * email getter
	 * @return email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * email setter
	 * @param email
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * userName getter
	 * @return userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * userName setter
	 * @param userName
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * password getter
	 * @return password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * password setter
	 * @param password
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * bills getter
	 * @return bills
	 */
	public List<Bill> getBills() {
		return bills;
	}

	/**
	 * bills setter
	 * @param bills
	 */
	public void setBills(List<Bill> bills) {
		this.bills = bills;
	}
	

}
