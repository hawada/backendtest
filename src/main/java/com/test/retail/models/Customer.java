package com.test.retail.models;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Customer Class
 * 
 * @author Hasan
 *
 */
public class Customer extends User {

	private Date creationDate;

	/**
	 * default constructor
	 */
	public Customer() {

	}

	/**
	 * Customer constructor
	 * 
	 * @param firstName
	 * @param lastName
	 * @param email
	 * @param userName
	 * @param password
	 * @param bills
	 * @param date
	 */
	public Customer(String firstName, String lastName, String email, String userName, String password, List<Bill> bills,
			Date date) {
		super(firstName, lastName, email, userName, password, bills);
		this.creationDate = date;
	}
	/**
	 * returns discount percentage
	 */
	double getDiscountPercentage() {
		return this.isCusomerCreationDateTwoYearsAgo() ? 0.05 : 0.0;
	}

	/**
	 * checks whether the customer creation date is before 2 years or not
	 * @return
	 */
	public boolean isCusomerCreationDateTwoYearsAgo() {
		if (creationDate == null)
			return false;

		Calendar todaysDateMinus2Years = Calendar.getInstance();
		todaysDateMinus2Years.add(Calendar.YEAR, -2);

		return creationDate.before(todaysDateMinus2Years.getTime());
	}

}