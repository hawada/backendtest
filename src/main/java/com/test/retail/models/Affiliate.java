package com.test.retail.models;

import java.util.List;

/**
 * Affiliate Class
 * @author Hasan
 *
 */
public class Affiliate extends User{

	/**
	 * Affiliate empty constructor
	 */
	public Affiliate() {
	
	}
	
	/**
	 * Affiliate constructor
	 * @param firstName
	 * @param lastName
	 * @param email
	 * @param userName
	 * @param password
	 * @param bills
	 */
	public Affiliate(String firstName, String lastName, String email, String userName, String password, List<Bill> bills) {
		super(firstName, lastName, email, userName, password, bills);
	}
	
	/**
	 * returns affiliate discount percentage
	 */
	@Override
	double getDiscountPercentage() {
		return 0.1;
	}
	
}