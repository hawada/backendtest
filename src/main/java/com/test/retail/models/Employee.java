package com.test.retail.models;

import java.util.List;
/**
 * Employee Class
 * @author Hasan
 *
 */
public class Employee extends User{
	
	/**
	 * Employee empty constructor
	 */
	public Employee() {
	
	}
	
	/**
	 * Employee constructor
	 * @param firstName
	 * @param lastName
	 * @param email
	 * @param userName
	 * @param password
	 * @param bills
	 */
	public Employee(String firstName, String lastName, String email, String userName, String password, List<Bill> bills) {
		super(firstName, lastName, email, userName, password, bills);
	}
	
	/**
	 * returns employee discount percentage
	 */
	@Override
	double getDiscountPercentage() {
		return 0.3;
	}

}
