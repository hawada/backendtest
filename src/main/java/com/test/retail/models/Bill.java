package com.test.retail.models;

import java.util.ArrayList;
import java.util.List;

import com.test.retail.utilities.ProductType;


/**
 * Bill Class
 * @author Hasan
 *
 */
public class Bill {

	private List<Product> products;
	private User owner;
	
	/**
	 * Bill empty constructor
	 */
	public Bill() {

	}

	/**
	 * add product method
	 * @param product
	 */
	public void addProduct(Product product) {
		this.products.add(product);
	}
	
	/**
	 * Bill constructor
	 * @param owner
	 */
	public Bill(User owner) {
		super();
		this.owner = owner;
		this.products = new ArrayList<>();
	}

	/**
	 * products getter
	 * @return products
	 */
	public List<Product> getProducts() {
		return products;
	}

	/**
	 * products setter
	 * @param products
	 */
	public void setProducts(List<Product> products) {
		if(products == null)
			this.products = new ArrayList<>();
		else
			this.products = products;
	}

	/**
	 * total before discount calculator
	 * @return totalBeforeDiscount
	 */
	public double getTotalBeforeDiscount() {
		return this.products.stream().mapToDouble(e -> e.getProductPrice()).sum();
	}

	/**
	 * total after discount calculator
	 * @return totalAfterDiscount
	 */
	public double getTotalAfterDiscount() {
		double totalAfterDiscount = this.products.stream().filter(p -> p.getProductType() != ProductType.GROCERY).mapToDouble(p -> p.getProductPrice() - (p.getProductPrice() * owner.getDiscountPercentage())).sum();
		totalAfterDiscount += this.products.stream().filter(p -> p.getProductType() == ProductType.GROCERY).mapToDouble(p -> p.getProductPrice()).sum();
		return totalAfterDiscount;
	}
	
	/**
	 * final total after discounts calculator
	 * @return total
	 */
	public double getFinalTotal() {
		double percentageDiscount = getTotalAfterDiscount();
		int numberOfHundreds = (int) (percentageDiscount / 100);
		
		return percentageDiscount - (numberOfHundreds * 5); 
	}

	/**
	 * owner getter
	 * @return owner
	 */
	public User getOwner() {
		return owner;
	}	
	
}
