package com.test.retail;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import com.test.retail.models.Bill;
import com.test.retail.models.Employee;
import com.test.retail.models.Product;
import com.test.retail.models.User;
import com.test.retail.utilities.Currency;
import com.test.retail.utilities.ProductType;

public class EmployeeTest {

	@Test
	public void testCreateEmployeeWithBill() {
		User employee = new Employee("Hasan", "Awada", "HAwada", "hasan_ewada@hotmail.com", "123456", null);
		Product product1 = new Product("Pepsi", 100, ProductType.COKIES, Currency.USD);
		Product product2 = new Product("Kinder", 250, ProductType.COKIES, Currency.USD);
		
		Bill bill = new Bill(employee);
		bill.addProduct(product1);
		bill.addProduct(product2);
		
		employee.getBills().add(bill);
		
		assertEquals(employee, bill.getOwner());
	}
	
	@Test
	public void testBillWith2Cokies() {
		User employee = new Employee("Hasan", "Awada", "HAwada", "hasan_ewada@hotmail.com", "123456", null);
		Product product1 = new Product("Pepsi", 100, ProductType.COKIES, Currency.USD);
		Product product2 = new Product("Kinder", 250, ProductType.COKIES, Currency.USD);
		
		Bill bill = new Bill(employee);
		bill.addProduct(product1);
		bill.addProduct(product2);
		
		assertEquals(235.0, bill.getFinalTotal(), 0);
	}
	
	@Test
	public void testBillWith1Cokie1Grocery() {
		User employee = new Employee("Hasan", "Awada", "HAwada", "hasan_ewada@hotmail.com", "123456", null);
		Product product1 = new Product("Pepsi", 100, ProductType.COKIES, Currency.USD);
		Product product2 = new Product("test", 250, ProductType.GROCERY, Currency.USD);
		
		Bill bill = new Bill(employee);
		bill.addProduct(product1);
		bill.addProduct(product2);
		
		assertEquals(305.0, bill.getFinalTotal(), 0);
	}
	
	@Test
	public void testBillWithMultipleCurrencies() {
		User employee = new Employee("Hasan", "Awada", "HAwada", "hasan_ewada@hotmail.com", "123456", null);
		Product product1 = new Product("Pepsi", 100, ProductType.COKIES, Currency.USD);
		Product product2 = new Product("test", 375000, ProductType.GROCERY, Currency.LB);
		
		Bill bill = new Bill(employee);
		bill.addProduct(product1);
		bill.addProduct(product2);
		
		assertEquals(305.0, bill.getFinalTotal(), 0);
	}

}
