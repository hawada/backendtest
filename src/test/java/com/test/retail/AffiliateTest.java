package com.test.retail;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.test.retail.models.Affiliate;
import com.test.retail.models.Bill;
import com.test.retail.models.Product;
import com.test.retail.models.User;
import com.test.retail.utilities.Currency;
import com.test.retail.utilities.ProductType;

public class AffiliateTest {

	@Test
	public void testBillWith2Cokies() {
		User affiliate = new Affiliate("Hasan", "Awada", "HAwada", "hasan_ewada@hotmail.com", "123456", null);
		
		
		Product product1 = new Product("Pepsi", 100, ProductType.COKIES, Currency.USD);
		Product product2 = new Product("Kinder", 250, ProductType.COKIES, Currency.USD);
		
		Bill bill = new Bill(affiliate);
		bill.addProduct(product1);
		bill.addProduct(product2);
		
		affiliate.getBills().add(bill);
		
		assertEquals(300.0, bill.getFinalTotal(), 0);
	}
	
	@Test
	public void testBillWith1Cokie1Grocery() {
		User affiliate = new Affiliate("Hasan", "Awada", "HAwada", "hasan_ewada@hotmail.com", "123456", null);
		Product product1 = new Product("Pepsi", 100, ProductType.COKIES, Currency.USD);
		Product product2 = new Product("test", 250, ProductType.GROCERY, Currency.USD);
		
		Bill bill = new Bill(affiliate);
		bill.addProduct(product1);
		bill.addProduct(product2);
		
		assertEquals(325.0, bill.getFinalTotal(), 0);
	}
}
