package com.test.retail;

import static org.junit.Assert.assertEquals;

import java.util.Calendar;

import org.junit.Test;

import com.test.retail.models.Bill;
import com.test.retail.models.Customer;
import com.test.retail.models.Product;
import com.test.retail.models.User;
import com.test.retail.utilities.Currency;
import com.test.retail.utilities.ProductType;

public class CustomerTest {

	@Test
	public void testBillWith2Cokies() {
		Calendar todaysDateMinus2Years = Calendar.getInstance();
	    todaysDateMinus2Years.add(Calendar.YEAR, -3);
		User customer = new Customer("Hasan", "Awada", "HAwada", "hasan_ewada@hotmail.com", "123456", null, todaysDateMinus2Years.getTime());
		
		
		Product product1 = new Product("Pepsi", 100, ProductType.COKIES, Currency.USD);
		Product product2 = new Product("Kinder", 250, ProductType.COKIES, Currency.USD);
		
		Bill bill = new Bill(customer);
		bill.addProduct(product1);
		bill.addProduct(product2);
		
		customer.getBills().add(bill);
		
		assertEquals(317.5, bill.getFinalTotal(), 0);
	}
	
	@Test
	public void test1Cokie1Grocery() {
		Calendar todaysDateMinus2Years = Calendar.getInstance();
	    todaysDateMinus2Years.add(Calendar.YEAR, -1);
		User customer = new Customer("Hasan", "Awada", "HAwada", "hasan_ewada@hotmail.com", "123456", null, todaysDateMinus2Years.getTime());
		
		
		Product product1 = new Product("Pepsi", 100, ProductType.COKIES, Currency.USD);
		Product product2 = new Product("test", 250, ProductType.GROCERY, Currency.USD);
		
		Bill bill = new Bill(customer);
		bill.addProduct(product1);
		bill.addProduct(product2);
		
		customer.getBills().add(bill);
		
		assertEquals(335.0, bill.getFinalTotal(), 0);
	}
	
	
}
